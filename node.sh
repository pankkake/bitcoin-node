#!/bin/bash -xeu
sed -i "s/wheezy\|squeeze/jessie/g" "/etc/apt/sources.list"

ls -l "/etc/apt/preferences.d/unstable" \
    || cat > "/etc/apt/preferences.d/unstable" << "END"
Package: libminiupnpc10
Pin: release a=unstable
Pin-Priority: 600

Package: bitcoind
Pin: release a=unstable
Pin-Priority: 600

Package: *
Pin: release a=jessie
Pin-Priority: 900

Package: *
Pin: release a=unstable
Pin-Priority: -10

END


ls -l "/etc/apt/sources.list.d/unstable.list" \
    || sed "s/jessie/unstable/g" "/etc/apt/sources.list" \
        | grep -v "updates\|security\|backports" \
        > "/etc/apt/sources.list.d/unstable.list"

apt-get update --assume-yes
apt-get dist-upgrade --assume-yes
apt-get autoremove --assume-yes

dpkg-query -l bitcoind || apt-get install bitcoind

id bitcoin || useradd -U -m bitcoin
BITHOME=~bitcoin
ls -ld $BITHOME

PW=$(dd if=/dev/urandom count=100 2> /dev/null | sha1sum -b - | head -c 40)
mkdir -pv "${BITHOME}/.bitcoin"
ls -l "${BITHOME}/.bitcoin/bitcoin.conf" \
    || cat > "${BITHOME}/.bitcoin/bitcoin.conf" << END
disablewallet=1
printtoconsole=1
rpcuser=user
rpcpassword=${PW}

END


chown -Rc bitcoin:bitcoin "${BITHOME}"

ls -l "/etc/default/bitcoind" \
    || cat > "/etc/default/bitcoind" << END
START_ARGS="--user bitcoin --chuid bitcoin --background --make-pidfile"
DAEMON_ARGS="--datadir=${BITHOME}/.bitcoin"
STOP_ARGS="--user bitcoin"

END


ls -l "/etc/init.d/bitcoind" \
    || cat > "/etc/init.d/bitcoind" << "END"
#!/bin/sh
# kFreeBSD do not accept scripts as interpreters, using #!/bin/sh and sourcing.
if [ true != "$INIT_D_SCRIPT_SOURCED" ] ; then
    set "$0" "$@"; INIT_D_SCRIPT_SOURCED=true . /lib/init/init-d-script
fi
### BEGIN INIT INFO
# Provides:          bitcoind
# Required-Start:    
# Required-Stop:     
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: bitcoind
# Description:       Start bitcoind node
### END INIT INFO
DESC="bitcoind"
DAEMON="/usr/bin/bitcoind"

END


chmod +x "/etc/init.d/bitcoind"

update-rc.d bitcoind defaults

service bitcoind start
