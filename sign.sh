#!/bin/bash -xeu
cd $(dirname $0)
SHA1=$(sha1sum node.sh)
echo "Running sha1sum node.sh should yield:" > verify
echo $SHA1 >> verify
gpg --default-key=0x651BB507D9889CC0 --clearsign verify
rm -vf verify
mv verify.asc VERIFY
