Bitcoin-node script
===================

This is a script that will set up a Bitcoin node on a Debian server.

It is designed for a brand-new Debian install, preferably Jessie or Squeeze.
It will try to upgrade to Jessie.

The wallet features will be disabled for security and performance reasons.

The script is idempotent.


Requirements
------------

* At least 30 GB of storage
* 512 MB of memory (swap should be OK)
* Debian 6 to 8


Installing
----------

Open a root SSH shell to your server.
Type in::

    wget https://bitbucket.org/pankkake/bitcoin-node/raw/master/node.sh
    bash -xeu node.sh


To view the status of your Bitcoin node::

    su bitcoin -c 'bitcoind getinfo'


Verifying the script authenticity
---------------------------------

See the VERIFY file.


Credits
-------

If you like this script, please tip 1Md5kvYempaW3NkYxdYbFezL6mKg7QwERe.
